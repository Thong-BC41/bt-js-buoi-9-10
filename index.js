var DSNV=[]

var dataJson = localStorage.getItem("DSNV_LOCAL");

if (dataJson !=null){
    var dataArr=JSON.parse(dataJson)
    DSNV= dataArr.map(function(item){
        var nv= new NhanVien(
            item.maNV,
            item.tenNV,
            item.emailNV,
            item.passNV,
            item.ngaylam,
            item.luongCB,
            item.loaiNV,
            item.giolam
        )
        return nv
    })
    renderDSNV(DSNV)
}


// Ẩn button cập nhật khi thêm mới NV
function them(){
  document.getElementById('btnCapNhat').style.display="none"
}

//Thêm nhân viên
function themNV(){   
    var nv=layThongTinTuForm()
    isValid=
    kiemTraTrung(nv.maNV,DSNV) &&
    kiemTraDoDai(nv.maNV,"tbTKNV",4,6)&&
    kiemTraKiSo(nv.maNV)

    isValid = isValid & kiemTraTen(nv.tenNV)& 
    kiemTraEmail(nv.emailNV)&
    kiemTraMatKhau(nv.passNV)&
    kiemTraDate(nv.ngaylam) &
    kiemTraLuongCB(nv.luongCB)&
    kiemTraChucvu(nv.loaiNV)&
    KiemTraGioLam(nv.giolam)
    
   if(isValid==true){

     DSNV.push(nv)
     
     //convert array DSNV thành json
     var dsnvJson=JSON.stringify(DSNV)
     
     //lưu json vào localtorage
     localStorage.setItem("DSNV_LOCAL",dsnvJson)
     
     //render ra danh sách nhân viên
     renderDSNV(DSNV)
    }
    
}

//xóa NV
function xoaNV(idNV) {
    // splice(viTri,1)
    var viTri = timKiemViTri(idNV, DSNV);
    console.log(viTri)
    if (viTri != -1) {
      DSNV.splice(viTri, 1);
      renderDSNV(DSNV);
    }
  }
  
  //Sửa Nhân Viên
  function suaNV(idNV) {
    var viTri = timKiemViTri(idNV, DSNV);  
    if (viTri != -1) {
      var nv = DSNV[viTri];  
      document.getElementById("tknv").value=nv.maNV
      document.getElementById("name").value=nv.tenNV    
      document.getElementById("email").value=nv.emailNV
      document.getElementById("password").value=nv.passNV
      document.getElementById("datepicker").value=nv.ngaylam
      document.getElementById("luongCB").value=nv.luongCB
      document.getElementById("chucvu").value=nv.loaiNV
      document.getElementById("gioLam").value=nv.giolam
      document.getElementById('index').value=viTri
      document.getElementById("tknv").disabled = true;
    }
    document.getElementById('btnThemNV').style.display="none"
   
}

// //Cập nhật 
function capnhatNV(){ 
  var nv=layThongTinTuForm()
  issValid=
  kiemTraTen(nv.tenNV)& 
  kiemTraEmail(nv.emailNV)&
  kiemTraMatKhau(nv.passNV)&
  kiemTraDate(nv.ngaylam) &
  kiemTraLuongCB(nv.luongCB)&
  kiemTraChucvu(nv.loaiNV)&
  KiemTraGioLam(nv.giolam)
   
 if(issValid==true){
  var viTri=document.getElementById('index').value
  DSNV[viTri]={
    maNV: document.getElementById("tknv").value,
    tenNV : document.getElementById("name").value,
    emailNV: document.getElementById("email").value,
    passNV : document.getElementById("password").value,
    loaiNV : document.getElementById("chucvu").value,
    ngaylam : document.getElementById("datepicker").value,
    luongCB : document.getElementById("luongCB").value*1,
    giolam : document.getElementById("gioLam").value*1
    }
    var dsnvJson=JSON.stringify(DSNV)
    localStorage.setItem("DSNV_LOCAL",dsnvJson)
    location.reload();
    renderDSNV(DSNV)
  }
    
}
// load lại khi đóng
function dongTAB(){
  location.reload()
}
 
//tìm kiếm loại nhân viên
//Cách 1
function timKiem(){
  var search=document.getElementById('searchName').value
  var nvSearch=DSNV.filter(value =>{
     return value.xeploai().toUpperCase().includes(search.toUpperCase())
  })
  renderDSNV(nvSearch)

}

//Cách 2
// function timKiem(){
//   var search=document.getElementById('searchName').value
//   var listdanhgiaNV=this.DSNV
//   var danhgiaNVArr=[]
//    if(timKiem){
//     for (var i=0; i<listdanhgiaNV.length ; i++){
//       var str=listdanhgiaNV[i].xeploai()
//       if( str.toUpperCase().includes(search.toUpperCase())){
//         var itemsearch={
//         maNV: listdanhgiaNV[i].maNV,
//         tenNV: listdanhgiaNV[i].tenNV,
//         emailNV: listdanhgiaNV[i].emailNV,
//         passNV: listdanhgiaNV[i].passNV,
//         loaiNV : listdanhgiaNV[i].loaiNV,
//         ngaylam :listdanhgiaNV[i].ngaylam,
//         luongCB :listdanhgiaNV[i].luongCB,
//         giolam :listdanhgiaNV[i].giolam,
//         }
//         danhgiaNVArr.push(itemsearch)
//         console.log(itemsearch)
//       }
//     }
//    }
  
// }