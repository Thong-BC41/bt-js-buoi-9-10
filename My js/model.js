function NhanVien(
    _maNV,
    _tenNV,
    _emailNV,
    _passNV,
    _ngaylam,
    _luongCB,
    _loaiNV,
    _giolam,

){
    this.maNV= _maNV
    this.tenNV= _tenNV
    this.emailNV= _emailNV
    this.passNV= _passNV
    this.ngaylam = _ngaylam
    this.luongCB= _luongCB
    this.loaiNV = _loaiNV
    this.giolam= _giolam
    this.Tongluong=function(){
        if (this.loaiNV=='Sếp'){
            return (this.luongCB*3).toLocaleString()
           }else if(this.loaiNV=='Trưởng phòng'){
            return (this.luongCB*2).toLocaleString()
           } else if(this.loaiNV=='Nhân viên'){
            return (this.luongCB*1).toLocaleString()
           }
    }
    this.xeploai=function(){
        if(this.giolam<160){
            return 'Nhân viên trung bình'
        }else if(this.giolam<176){
            return 'Nhân Viên Khá'
        }else if(this.giolam<192){
            return 'Nhân Viên Giỏi'
        }else if(this.giolam>=192){
            return 'Nhân Viên Xuất Sắc'
        } 
    }

}