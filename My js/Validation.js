
function kiemTraTrung(idNV,nvArr){
    //findIndex return vị trí của item nếu điều kiện true, nếu không tìm thấy trả về -1
    var viTri=nvArr.findIndex(function(item){
        return item.maNV==idNV
    })
    if (viTri !=-1){
        document.getElementById('tbTKNV').style.display="inline-block"
        document.getElementById('tbTKNV').innerHTML="Mã nhân viên đã tồn tại"
        return false
    }else{
        document.getElementById('tbTKNV').innerHTML=""
        return true 
    } 
}

function kiemTraDoDai(value,idErr,min,max){
    var length=value.length
    if(value==null || value==""){
        document.getElementById('tbTKNV').style.display="inline-block"
        document.getElementById(idErr).innerText=`Không được để trống`
        return false
    }else if( length<min || length>max){
        document.getElementById('tbTKNV').style.display="inline-block"
        document.getElementById(idErr).innerText=`Độ dài phải từ ${min} đến ${max} kí tự`
        return false
    }else{
        document.getElementById(idErr).innerText=""
        return true
    }
}

function kiemTraKiSo(value){
    var re = /^\d+$/;
    var isNumber=re.test(value) 
    if(isNumber){
        document.getElementById('tbTKNV').innerText=""
        return true
    }else{
        document.getElementById('tbTKNV').style.display="inline-block"
        document.getElementById('tbTKNV').innerText="Phải là số"
        return false
    }
}

function kiemTraTen(value){
    var re=/^[A-Za-z ]+$/
    var isTen=re.test(value)
    if(value==null || value==""){
        document.getElementById('tbTen').style.display="inline-block"
        document.getElementById("tbTen").innerText="Không được để trống"
        return false
    }else if(isTen){
        document.getElementById("tbTen").innerText=""
        return true
    }else{
        document.getElementById('tbTen').style.display="inline-block"
        document.getElementById("tbTen").innerText=`Tên nhân viên phải là chữ` 
        return false
    }
}

function kiemTraEmail(value){
    var re= /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    var isEmail=re.test(value)
    if(value==null || value==""){
        document.getElementById('tbEmail').style.display="inline-block"
        document.getElementById('tbEmail').innerText="Không được để trống"
        return false
    }else if(isEmail){
        document.getElementById('tbEmail').innerText=""
        return true
    }else{
        document.getElementById('tbEmail').style.display="inline-block"
        document.getElementById('tbEmail').innerText="Email không hợp lệ" 
        return false
    }
}

function kiemTraMatKhau(value){
    var re=/^(?=.*[A-Z])(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{6,10}$/
    var isPass=re.test(value)
    if(value==null || value==""){
        document.getElementById('tbMatKhau').style.display="inline-block"
        document.getElementById('tbMatKhau').innerHTML="Không được để trống"
        return false
    }else if(isPass){
        document.getElementById('tbMatKhau').innerHTML=""
        return true
    }else {
        document.getElementById('tbMatKhau').style.display="inline-block"
        document.getElementById('tbMatKhau').innerHTML=`mật Khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)`
        return false
    }
}

function kiemTraDate(value){
    var re= /^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/;
    var isDate=re.test(value)
    if(value==null || value==""){
        document.getElementById('tbNgay').style.display="inline-block"
        document.getElementById('tbNgay').innerHTML="Không được để trống"
        return false
    }else if(isDate){
        document.getElementById('tbNgay').innerHTML=""
        return true
    }else{
        document.getElementById('tbNgay').style.display="inline-block"
        document.getElementById('tbNgay').innerHTML="Date không hợp lệ"
        return false
    }   
}

function kiemTraLuongCB(value){
    var re = /^\d+$/;
    var isNumber=re.test(value)
    if(value==null || value==""){
        document.getElementById('tbLuongCB').style.display="inline-block"
        document.getElementById('tbLuongCB').innerHTML="Không được để trống"
        return false
    }else{
        if(isNumber){
            if(value<1000000 || value>20000000){
                document.getElementById('tbLuongCB').style.display="inline-block"
                document.getElementById('tbLuongCB').innerHTML="Lương cơ bản 1.000.000 đến 20.000.000"
                return false
             }else{
                document.getElementById('tbLuongCB').innerHTML=""
                return true
             }
        }else{
            document.getElementById('tbLuongCB').style.display="inline-block"
            document.getElementById("tbLuongCB").innerText="Lương cơ bản phải là số "
        return false
        }
    }
  
}

function kiemTraChucvu(value){
    if(value=="Chọn chức vụ"){
        document.getElementById('tbChucVu').style.display="inline-block"
        document.getElementById('tbChucVu').innerHTML=`Chưa chọn chức vụ`
        return false
    }else{
        document.getElementById('tbChucVu').innerHTML=""
        return true
    }
}


function KiemTraGioLam(value){
    var re = /^\d+$/;
    var isNumber=re.test(value)
    if(value==null || value==""){
        document.getElementById('tbGiolam').style.display="inline-block"
        document.getElementById('tbGiolam').innerHTML="Không được để trống"
        return false
    }else{
        if(isNumber){
            if(value<80 || value>200){
                document.getElementById('tbGiolam').style.display="inline-block"
                document.getElementById('tbGiolam').innerHTML="Số giờ làm 80-200 giờ"
                return false
             }else{
                document.getElementById('tbGiolam').innerHTML=""
                return true
             }
        }else{
            document.getElementById('tbGiolam').style.display="inline-block"
            document.getElementById("tbGiolam").innerText="giờ làm phải là số "
            return false
        }
    } 
}

